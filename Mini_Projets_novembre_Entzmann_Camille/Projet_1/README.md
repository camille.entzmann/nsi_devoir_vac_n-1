# Projet 1

Projet hébergé sur gitlab.com : https://gitlab.com/camille.entzmann/nsi_devoir_vac_n-1/-/tree/master/Mini_Projets_novembre_Entzmann_Camille/Projet_1



```
  _____           _      _     __ 
 |  __ \         (_)    | |   /_ |
 | |__) | __ ___  _  ___| |_   | |
 |  ___/ '__/ _ \| |/ _ \ __|  | |
 | |   | | | (_) | |  __/ |_   | |
 |_|   |_|  \___/| |\___|\__|  |_|
                _/ |              
               |__/               
```

Bannière générée sur [patorjk.com](http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Projet%201) police *Big*.


## Différentes étapes 

#### Etape 1 : 

```python
from random import*

allumettes = 21 

while allumettes > 0 :
    print(f"Il y a {allumettes} allumettes.")
    joueur = int(input("Combien en prenez-vous ?"))
    ordi = randint(1,3)
    print(f"Je prends {ordi} allumettes.")
    allumettes = allumettes - ordi - joueur 
```

#### Etape 2

```python
while allumettes > 0 :
    print(f"Il y a {allumettes} allumettes.")
    joueur = int(input("Combien en prenez-vous ?"))
    allumettes_j = allumettes - joueur 
    
    # pour que l'ordi ne retire pas plus d'allumettes qu'il n'en reste
    ordi = randint(1,min(allumettes_j,3))
    print(f"Je prends {ordi} allumettes.")
    allumettes = allumettes_j - ordi

    if allumettes == 0  : 
        print("Vous avez gagné !")
    if allumettes_j == 0 : 
        print("Vous avez perdu !")
```

#### Etape 3

```python
while allumettes > 0 :
    print(f"Il y a {allumettes} allumettes.")

    joueur = int(input("Combien en prenez-vous ?"))
    while not 1 <= joueur <= 3:
        joueur = int(input("Le nombre doit être compris entre 1 et 3. Combien en prenez-vous ?"))
    
    while joueur > allumettes : 
        joueur = int(input("Vous ne pouvez pas prendre plus d'allumettes qu'il n'en reste. Combien en prenez-vous ?"))
    
    allumettes = allumettes - joueur

    ordi = min(randint(1,3), allumettes)
    print(f"Je prends {ordi} allumettes.")

    allumettes = allumettes - ordi

print('La partie est terminée.')
if ordi == 0:
    print('Vous avez perdu !')
else:
    print('Vous avez gagné !')
```

#### Etape 4 

```python 
while allumettes > 0 :
    print(f"Il y a {allumettes} allumettes.")

    joueur = int(input("Combien en prenez-vous ?"))
    while not 1 <= joueur <= 3:
        joueur = int(input("Le nombre doit être compris entre 1 et 3. Combien en prenez-vous ?"))
    
    while joueur > allumettes : 
        joueur = int(input("Vous ne pouvez pas prendre plus d'allumettes qu'il n'en reste. Combien en prenez-vous ?"))
    
    allumettes = allumettes - joueur

    if joueur == 1 : 
        ordi = min(3,allumettes)
    elif joueur == 2 : 
        ordi = min(2,allumettes) 
    else :
        ordi = min(1,allumettes)
    
    #ordi = min(randint(1,3), allumettes)
    print(f"Je prends {ordi} allumettes.")

    allumettes = allumettes - ordi

print('La partie est terminée.')
if ordi == 0:
    print('Vous avez perdu !')
else:
    print('Vous avez gagné !')
```