from random import*

allumettes = 21 

#boucle marchant tant que le compte d'allumettes est supérieur à 0
while allumettes > 0 :
    print(f"Il y a {allumettes} allumettes.")

    # Tour du joueur :
    joueur = int(input("Combien en prenez-vous ?"))

    #boucle vérifiant que le joueur prends 1,2 ou 3 allumettes
    while not 1 <= joueur <= 3:
        print("Le nombre doit être compris entre 1 et 3.")
        joueur = int(input("Combien en prenez-vous ?"))
    
    #boucle vérifiant que le joueur ne prends pas plus d'allumettes qu'il n'en reste
    while joueur > allumettes : 
        print("Vous ne pouvez pas prendre plus d'allumettes qu'il n'en reste.")
        joueur = int(input("Combien en prenez-vous ?"))
    
    allumettes = allumettes - joueur

    # Tour de l'ordi : 
    if joueur == 1 : 
        ordi = min(3,allumettes)
    elif joueur == 2 : 
        ordi = min(2,allumettes) 
    else :
        ordi = min(1,allumettes)
    
    #ordi = min(randint(1,3), allumettes)
    print(f"Je prends {ordi} allumettes.")

    allumettes = allumettes - ordi

print('La partie est terminée.')
if ordi == 0:
    print('Vous avez perdu !')
else:
    print('Vous avez gagné !')



