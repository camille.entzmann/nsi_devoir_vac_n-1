# Projet 2

Projet hébergé sur gitlab.com : https://gitlab.com/camille.entzmann/nsi_devoir_vac_n-1/-/tree/master/Mini_Projets_novembre_Entzmann_Camille/Projet_2

```
  _____           _      _     ___  
 |  __ \         (_)    | |   |__ \ 
 | |__) | __ ___  _  ___| |_     ) |
 |  ___/ '__/ _ \| |/ _ \ __|   / / 
 | |   | | | (_) | |  __/ |_   / /_ 
 |_|   |_|  \___/| |\___|\__| |____|
                _/ |                
               |__/                 
```

## Différentes étapes

#### Etape 1 : Def milieu, initialisation variables et triangle

```python
from turtle import*
from random import*

speed(5)
reset()
clear()

def milieu(x1,y1,x2,y2):
    xM = (x1 + x2)/2
    yM = (y1 + y2)/2
    return(xM,yM)

xA,yA = 200, 200
xB,yB = -200, 200 
xC,yC = 0, -200
xR,yR = 100, 300

up()
goto(xA,yA)
down()
goto(xB,yB)
goto(xC,yC)
goto(xA,yA)
up()
goto(xR,yR)

mainloop()
```

#### Etape 2 : un déplacement de Rantanplan 

```python 
choix = randint(1,3)

if choix == 1 :
    xR,yR = milieu(xR,yR,xA,yA)
elif choix == 2 :
    xR,yR = milieu(xR,yR,xB,yB)
else : 
    xR,yR = milieu(xR,yR,xC,yC)

goto(xR,yR)
down()
circle(1)
up()
```

#### Etape 3 : positions aléatoires + plusieurs déplacements de Rantanplan 

```python
xA,yA = randint(-400, 400), randint(-300,300)
xB,yB = randint(-400, 400), randint(-300,300)
xC,yC = randint(-400, 400), randint(-300,300)
xR,yR = randint(-400, 400), randint(-300,300)

up()
goto(xA,yA)
down()
goto(xB,yB)
goto(xC,yC)
goto(xA,yA)
up()
goto(xR,yR)

for k in range(30):
    choix = randint(1,3)

    if choix == 1 :
        xR,yR = milieu(xR,yR,xA,yA)
    elif choix == 2 :
        xR,yR = milieu(xR,yR,xB,yB)
    else : 
        xR,yR = milieu(xR,yR,xC,yC)

    goto(xR,yR)
    down()
    circle(1)
    up()

mainloop()
```