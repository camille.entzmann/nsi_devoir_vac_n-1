from turtle import*
from random import*

speed(0)
reset()
clear()

#fonction qui retourne la position du milieu d'un segment
def milieu(x1,y1,x2,y2):
    xM = (x1 + x2)/2
    yM = (y1 + y2)/2
    return(xM,yM)

#randint permet de donner des positions aléatoires aux Daltons et a Rantanplan
xA,yA = randint(-400, 400), randint(-300,300)
xB,yB = randint(-400, 400), randint(-300,300)
xC,yC = randint(-400, 400), randint(-300,300)
xR,yR = randint(-400, 400), randint(-300,300)

#trace le triangle formé par les Daltons 
up()
goto(xA,yA)
down()
goto(xB,yB)
goto(xC,yC)
goto(xA,yA)
up()
#va à la position de Rantanplan
goto(xR,yR)

#boucle déterminant le nombre de déplacements de Rantanplan
for k in range(30):
    choix = randint(1,3)#1,2,3 sont les 3 frères Daltons (Joe,Jack,William)

    if choix == 1 :
        xR,yR = milieu(xR,yR,xA,yA)#va au milieu du segment [Rantanplan,Joe]
    elif choix == 2 :
        xR,yR = milieu(xR,yR,xB,yB)#pareil pour [Rantanplan,Jack]
    else : 
        xR,yR = milieu(xR,yR,xC,yC)#pareil pour [Rantanplan,William]

    goto(xR,yR)
    down()
    circle(1)#fais un cercle (trou) au milieu du segment
    up()

mainloop()