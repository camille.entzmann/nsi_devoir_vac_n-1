# Projet 3

```
  _____           _      _     ____  
 |  __ \         (_)    | |   |___ \ 
 | |__) | __ ___  _  ___| |_    __) |
 |  ___/ '__/ _ \| |/ _ \ __|  |__ < 
 | |   | | | (_) | |  __/ |_   ___) |
 |_|   |_|  \___/| |\___|\__| |____/ 
                _/ |                 
               |__/      
```

# Différentes étapes 


On veut d'abord obtenir la table de multiplication suivante :
```
2 * 0 = 0
2 * 1 = 2
2 * 2 = 4
2 ...
2 * 9 = 18
```

```python
for i in range(n):
    print(f"2 x {i} = {2*i}")
```


# Explication trigonométrique

![](https://www.educastream.com/IMG/Image/fonctions-cosinus-sinus-02b.png)

![](https://www.educastream.com/IMG/Image/fonctions-cosinus-sinus-03c.png)

Périmètre d'un cerle : 2 * pi * r


|    n  | angle |
|---|---|
|    0  | 0 |
|  1  | 2pi / 10 * 1 |
|  2  | 2pi / 10 * 2 |
| ... | ... |
|  i  | 2pi / n * i |
| ... | ... |
|  5  | pi |
| ... | ... |
|  10 | 2pi = 0 |


#### Table 2, modulo 10 :
![](table2_n10.png)
#### Table 4, modulo 300 :
![](table4_n300.png)
#### Table 25, modulo 39 :
![](table25_n39.png)