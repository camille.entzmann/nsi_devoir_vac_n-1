from turtle import *  
from math import *

speed(0)
reset()
clear()

# rayon du cercle
r = 200

# on étudie la table du 2
table = 25
# la taille de la table
n = 39

up()
# bouger la tortue pour centrer le cercle
goto(0 ,-200 )
# on trace le cercle
down()
circle(r)
# remette la tortue au centre
up()
goto(0,0)

# une boucle pour placer les points
for i in range(n):
    # calcul des coordonnées du point courant
    # calcul de l'abscisse
    xi = r * cos(2 * pi / n *i)
    # calcul de l'ordonnée
    yi = r * sin(2 * pi / n *i)
    # on y va et on place un point
    goto(xi,yi)
    dot(5)


# une boucle pour chaque ligne de la table de multiplication
for i in range(n):
    print(f"2 x {i} = {2*i}")

    calcul = table * i

    # se placer sur le premier point
    # calcul de l'abscisse
    xi = r * cos(2 * pi / n * i)
    # calcul de l'ordonnée
    yi = r * sin(2 * pi / n * i)
    up()
    goto(xi,yi)


    # calcul des coordonnées du point courant
    # calcul de l'abscisse
    xi = r * cos(2 * pi / n * calcul)
    # calcul de l'ordonnée
    yi = r * sin(2 * pi / n * calcul)
    # on y va et on place un point
    down()
    goto(xi,yi)


cv = getcanvas()
cv.postscript(file=f"table{table}_n{n}.ps", colormode='color')

mainloop()