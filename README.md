# Projet vacances

![xxxx](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Python.svg/240px-Python.svg.png)

Voir le [sujet](image.pdf) donné par la prof 

Il y a 3 projets :
- Projet [1](Mini_Projets_novembre_Entzmann_Camille/Projet_1/README.md)
- Projet [2](Mini_Projets_novembre_Entzmann_Camille/Projet_2/README.md)
- Projet [3](Mini_Projets_novembre_Entzmann_Camille/Projet_3/README.md)

J'ai choisi les projets [1](Mini_Projets_novembre_Entzmann_Camille/Projet_1/README.md) et [2](Mini_Projets_novembre_Entzmann_Camille/Projet_2/README.md) 



Projet hébergé sur gitlab.com : https://gitlab.com/camille.entzmann/nsi_devoir_vac_n-1


# Génération de la documentation

Utilisation de pandoc pour générer les fichiers PDF :

```
pandoc README.md -s --pdf-engine=xelatex -o README.pdf
```
